import Counter from '../components/Counter';
import * as actions from '../actions';
import { connect } from "react-redux";

export function getRandomColor() {
  const colors = [
    '#495057',
    '#f03e3e',
    '#d6336c',
    '#ae3ec9',
    '#7048e8',
    '#4263eb',
    '#1c7cd6'
  ];

  const random = Math.floor(Math.random() * 7);

  return colors[random]
}

const mapStateToProps = (state) => ({
  color: state.colorData.color,
  number: state.numberData.number
});

const mapDispatchToProps = (dispatch) => ({
  onIncrement: () => dispatch(actions.increment()),
  onDecrement: () => dispatch(actions.decrement()),
  onSetColor: () => {
    const color = getRandomColor();
    console.log(actions.setColor(color));
    dispatch(actions.setColor({color: color}))
  },
});

const CounterContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);

export default CounterContainer;