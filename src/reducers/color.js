import { handleActions } from 'redux-actions'

const color = handleActions({
  SET_COLOR: (state, action) => ({
    color: action.payload.color
  })
}, {color: 'black'});

export default color;