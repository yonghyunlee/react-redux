import { handleActions } from 'redux-actions'

const number = handleActions({
  INCREMENT: (state, action) => ({
    number: state.number + 1
  }),
  DECREMENT: (state, action) => ({
    number: state.number - 1
  })
}, {number: 0});

export default number;