import * as types from './ActionTypes'
import { createAction } from 'redux-actions'

export const increment = createAction(types.INCREMENT);

export const decrement = createAction(types.DECREMENT);

export const setColor = createAction(types.SET_COLOR);